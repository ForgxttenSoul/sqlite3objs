const sqlite3 = require("better-sqlite3");
class sqlobj {
    constructor(path = false) {
        this.db = {};
        if (path !== false) this.db = new sqlite3(path);
    }
    // ===============================================
    open(path = false) {
        if (this.isOpen()) return (false);
        if (!!path === true) {
            this.db = new sqlite3(path);
        }
        return (!!path);
    }
    close() {
        if (!this.isOpen()) return (false);
        this.db.close();
        this.db = {};
    }
    // ===============================================
    dbPath() {
        return (this.isOpen() ? this.db.name : false);
    }
    // ===============================================
    isOpen() {
        return (this.db.open || false);
    }
    isBusy() {
        return (this.isOpen() ? this.db.inTransaction : false);
    }
    isMemory() {
        return (this.isOpen() ? this.db.readonly : false);
    }
    // ===============================================
    create(table_name, table_default) {
        if (!this.isOpen()) return (false);
        if (this.exists(table_name)) return (false);
        /*
            db.create("table1", { -> CREATE TABLE table1 (id integer primary key autoincrement, fname varchar(255) default '', lname varchar(255) default '')
                id: "integer primary key autoincrement",
                fname: "varchar(255) default ''",
                lname: "varchar(255) default ''"
            });
        */
        let params = [];
        Object.keys(table_default).forEach(key => {
            let def = table_default[key];
            switch (def) {
                case "<key>":
                    def = "integer primary key autoincrement";
                    break;
                case "<integer>":
                    def = "integer default 0";
                    break;
                case "<int>":
                    def = "integer default 0";
                    break;
                case "<string>":
                    def = "varchar(255) default ''";
                    break;
                case "<str>":
                    def = "varchar(255) default ''";
                    break;
                case "<boolean>":
                    def = "boolean default false";
                    break;
                case "<bool>":
                    def = "boolean default false";
                    break;
                case "<text>":
                    def = "text default ''"
            }
            params.push(`${key} ${def}`);
        });
        params = params.join(", ");
        const sql = `CREATE TABLE ${table_name} (${params})`;
        this.exec(sql);
        return (true);
    }
    purge(table_name) {
        if (!this.isOpen()) return (false);
        if (!this.exists(table_name)) return (false);
        /*
            db.purge("table1); -> DELETE FROM table1
        */
        const sql = `DELETE FROM ${table_name}`;
        this.exec(sql);
        return (true);
    }
    list() {
        if (!this.isOpen()) return (false);
        const sql = `SELECT name FROM sqlite_master WHERE type = 'table'`;
        const output = this.exec(sql);
        output.forEach((data, i) => data.name === "sqlite_sequence" ? output.splice(i, 1) : false);
        return (output);
    }
    exists(table_name) {
        if (!this.isOpen()) return (false);
        /*
            db.exists("table1"); -> SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'table1'
        */
        const sql = `SELECT name FROM sqlite_master WHERE type = 'table' AND name = ?`;
        const output = this.exec(sql, [table_name]);
        return (output.length > 0);
    }
    delete(table_name) {
        if (!this.isOpen()) return (false);
        if (!this.exists(table_name)) return (false);
        /*
            db.delete("table1"); -> DROP TABLE table1
        */
        const sql = `DROP TABLE ${table_name}`;
        this.exec(sql);
        return (true);
    }
    // ===============================================
    insert(table_name, row_data) {
        if (!this.isOpen()) return (false);
        if (!this.exists(table_name)) return (false);
        /*
            db.insert("table1", { -> INSERT INTO table1 (fname, lname) VALUES ('first_name', 'last_name')
                fname: "first_name",
                lname: "last_name"
            });
        */
        let data_keys = [];
        let data_values = [];
        const params = [];
        Object.keys(row_data).forEach(key => {
            data_keys.push(key);
            data_values.push("?");
            params.push(row_data[key]);
        });
        data_keys = data_keys.join(", ");
        data_values = data_values.join(", ");
        const sql = `INSERT INTO ${table_name} (${data_keys}) VALUES (${data_values})`;
        const output = this.exec(sql, params);
        return(!!output.changes);
    }
    update(table_name, where_data, new_data) {
        if (!this.isOpen()) return (false);
        if (!this.exists(table_name)) return (false);
        /*
            db.update("table1", { -> UPDATE table1 SET fname = 'first_name' WHERE id = 1
                id: 1
            }, {
                fname: "first_name"
            });
        */
        let set_where = [];
        let where = [];
        const params = [];
        Object.keys(new_data).forEach(key => {
            set_where.push(`${key} = ?`);
            params.push(new_data[key]);
        });
        Object.keys(where_data).forEach(key => {
            where.push(`${key} = ?`);
            params.push(where_data[key]);
        });
        where = where.join(" AND ");
        set_where = set_where.join(", ");
        const sql = `UPDATE ${table_name} SET ${set_where} WHERE ${where}`;
        const output = this.exec(sql, params);
        return (output.changes);
    }
    remove(table_name, where_data = false) {
        if (!this.isOpen()) return (false);
        if (!this.exists(table_name)) return (false);
        /*
            db.remove("table1"); -> DELETE FROM table1
            db.remove("table1", { -> DELETE FROM table1 WHERE id = 3
                id: 3
            });
        */
        if (where_data === false) {
            where_data = {
                1: 1
            };
        }
        let where = [];
        const params = [];
        Object.keys(where_data).forEach(key => {
            where.push(`${key} = ?`);
            params.push(where_data[key]);
        });
        where = where.join(" AND ");
        const sql = `DELETE FROM ${table_name} WHERE ${where}`;
        const output = this.exec(sql, params);
        return (output.changes);
    }
    select(table_name, opts = {}) {
        if (!this.isOpen()) return (false);
        if (!this.exists(table_name)) return (false);
        /*
            db.select("table1"); -> SELECT * FROM test1
            db.select("table1", { -> SELECT fname FROM test1
                fields: [
                    "fname"
                ]
            });
            db.select("table1", { -> SELECT * FROM test1 WHERE fname LIKE 'ftest1'
                where: {
                    fname: "ftest1"
                }
            });
            db.select("table1", { -> SELECT fname, lname FROM test1 WHERE fname LIKE 'ftest2'
                fields: [
                    "fname",
                    "lname"
                ],
                where: {
                    fname: "ftest2"
                }
            });
        */
        opts.where = opts.where || {
            1: 1
        };
        opts.fields = opts.fields || ["*"];
        let where = [];
        const params = [];
        Object.keys(opts.where).forEach(key => {
            where.push(`${key} LIKE ?`);
            params.push(opts.where[key]);
        });
        where = where.join(" AND ");
        const sql = `SELECT ${opts.fields.join(", ")} FROM ${table_name} WHERE ${where}`;
        const output = this.exec(sql, params);
        return (output);
    }
    // ===============================================
    exec(sql, params = []) {
        if (!this.isOpen()) return (false);
        let ss = this.db.prepare(sql).bind(...params);
        return (ss.returnsData ? ss.all() : ss.run());
    }
}
module.exports = sqlobj;