Here are just the functions underlying:

    const objdb = require("sqlite3objs");
    const db = objdb("/path/to/file.db");
    db.open("/path/to/file.db"); // If not open
    db.close(); // If open
    
    db.create("table1", { // creates a new table if it doesnt exist
        id: "INTEGER PRIMARY KEY AUTOINCREMENT",
        fname: "VARCHAR(255) DEFAULT ''",
        lname: "VARCHAR(255) DEFAULT ''",
    });
    db.purge("table1"); // purges table of all data
    db.exists("table1"); // check if table exists
    db.delete("table1"); // deletes table from database
    
    db.insert("table1", { // inserts data into a table
        fname: "first_name",
        lname: "last_name"
    });
    db.update("table1", { // updates data in a table
        id: 1
    }, {
        fname: "first_name_1"
    });
    db.remove("table1", { // deletes data from a table
        fname: "first_name"
    });
    db.select("table1", { // selects data from a table
        fields: ["fname", "lname"],
        where: {
            id: 1
        }
    });
    
    db.exec("SELECT * FROM table WHERE fname = ?", ["first_name"]); // executes prepared statement and binds parameters